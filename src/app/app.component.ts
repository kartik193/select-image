import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  imageArray: any = [];
  imageArray1: any = [
    "https://sidhis3.s3.us-east-2.amazonaws.com/100/download%20%281%29.jpeg",
    "https://sidhis3.s3.us-east-2.amazonaws.com/798/3.jpg",
    "https://sidhis3.s3.us-east-2.amazonaws.com/658/download%20%281%29hhh.jpg",
    "https://sidhis3.s3.us-east-2.amazonaws.com/117/download.jpeg",
    "https://sidhis3.s3.us-east-2.amazonaws.com/629/naval.jpg",
    "https://sidhis3.s3.us-east-2.amazonaws.com/930/dfvdfbffb.JPG"
  ];

  patchImg(indexValue: any, imgName: any) {
    if (this.imageArray1.length - 1 >= indexValue) {
      let arrayLength = this.imageArray.length;
      let value = this.imageArray1[this.imageArray.length];
      this.imageArray.push(this.imageArray1[indexValue]);
      this.imageArray1[arrayLength] = this.imageArray1[indexValue];
      this.imageArray1[indexValue] = value;
    }
  }

  deleteImg(deleteIdx: any, url: any) {
    console.log(deleteIdx);
    let urlIndex = this.imageArray1.indexOf("url");
    this.imageArray1.splice(urlIndex, 1);
    this.imageArray.splice(deleteIdx, 1);
    this.imageArray1.push(url);
  }
}